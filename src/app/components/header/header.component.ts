import {
	Component,
	OnInit,
} from '@angular/core';

import {
	Router,
	ActivatedRoute,
} from '@angular/router';

import {
	Title,
} from '@angular/platform-browser';

import {
	Store, ActionReducer,
} from '@ngrx/store';

import {
	TranslateService,
} from '@ngx-translate/core';

import {
	StateInterface,
} from 'store';

import {
	Moment,
} from 'utilities';

import {
	LocaleService,
} from 'services';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
	currentUrl: string;

	availableLanguages: string[] = [];
	currentLanguage: string;

	time: Moment.Moment = Moment();

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private titleService: Title,
		private store: Store<StateInterface>,
		private translate: TranslateService,
		private localeService: LocaleService,
	) {}

	ngOnInit(): void {
		this.route.url.subscribe((urlSegments: any) => {
			if (!urlSegments.length) {
				this.currentUrl = '/';
				return;
			}
			this.currentUrl = `/${urlSegments[0].path}`;
		});

		this.store.select('data', 'locale').subscribe((storeLocale: any) => {
			if (storeLocale) {
				this.currentLanguage = storeLocale.currentLocale;
			}
		});

		this.store.select('data', 'time').subscribe((storeTime: any) => {
			this.availableLanguages = this.localeService.getAvailableLocales();
			this.time = Moment(storeTime);
		});
	}

	setLanguage(language: string): void {
		this.localeService.setLocale(language);
	}
}
