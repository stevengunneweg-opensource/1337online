import {
	Component,
	OnInit,
	Input,
} from '@angular/core';

import {
	Store,
} from '@ngrx/store';

import {
	StateInterface,
} from 'store';

import {
	PostInterface,
	KnownTimespansEnum,
} from 'interfaces';

import {
	PostsService,
} from 'services';

@Component({
	selector: 'app-score-list',
	templateUrl: './score-list.component.html',
})
export class ScoreListComponent implements OnInit {
	@Input() title: string;
	@Input()
	get timespan(): string {
		return this._timespan;
	}
	set timespan(value: string) {
		this._timespan = value;
		this.getPostsList();
	}
	@Input() date?: string;
	@Input() username?: string;

	_timespan: string;
	posts: PostInterface[] = [];

	constructor(
		private store: Store<StateInterface>,
		private postsService: PostsService,
	) {
	}

	ngOnInit(): void {
		// console.log(this.username);

		this.store.select('data', 'posts').subscribe((storePosts: PostInterface[]) => {
			if (!storePosts) {
				storePosts = [];
			}
			storePosts.sort((postA: PostInterface, postB: PostInterface) => {
				let timeA: string = postA.time.datetime.substr(postA.time.datetime.indexOf(':'), postA.time.datetime.length);
				let timeB: string = postB.time.datetime.substr(postB.time.datetime.indexOf(':'), postB.time.datetime.length);

				if (timeA < timeB) { return -1; }
				if (timeA > timeB) { return 1; }
				return 0;
			});
			this.posts = storePosts;
		});
	}

	getPostsList(): void {
		this.postsService.getPosts(this.timespan, this.date).then((result: PostInterface[]) => {
			this.store.dispatch({
				type: 'PostsSet',
				payload: result,
			});
		}).catch((error: any) => {
			console.log(error);
		});
	}
}
