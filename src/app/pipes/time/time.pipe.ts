import {
	Pipe,
	PipeTransform,
} from '@angular/core';

/**
 * Pipe to isolate time from high precision time
 */
@Pipe({
	name: 'time',
})
export class TimePipe implements PipeTransform {

	transform(value: any): any {
		return value.match(/(?<=T).*(?=\+)/g);
	}
}
