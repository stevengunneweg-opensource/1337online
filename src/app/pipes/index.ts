export {
	MomentPipe,
} from './moment/moment.pipe';

export {
	TimePipe,
} from './time/time.pipe';
