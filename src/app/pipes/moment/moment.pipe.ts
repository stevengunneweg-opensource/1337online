import {
	Pipe,
	PipeTransform,
} from '@angular/core';

import {
	Moment,
} from 'utilities';

/**
 * Pipe to display dates in custom format
 */
@Pipe({
	name: 'moment',
	pure: false,
})
export class MomentPipe implements PipeTransform {
	/**
	 * Default date format
	 * @param {string} defaultDateFormat
	 */
	private defaultDateFormat: string = 'D MMMM YYYY';

	/**
	 * Transforms a date or dateString to the desired format
	 * @param {Moment.Moment|string} value
	 * @param {any} args
	 * @return {string}
	 */
	transform(value: (Moment.Moment | string), args?: any): string {
		return Moment(value || undefined).format(args || this.defaultDateFormat);
	}
}
