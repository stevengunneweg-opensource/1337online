import {
	MomentPipe,
} from './moment.pipe';

import {
	Moment,
} from 'utilities';

describe('Pipes: MomentPipe', () => {
	let pipe: MomentPipe;

	beforeEach(() => {
		pipe = new MomentPipe();
	});

	// Default behaviour
	it('should transform dates', () => {
		expect(pipe.transform(null, 'YYYY')).toEqual('' + (new Date()).getFullYear());
		expect(pipe.transform(Moment('2013-01-01T00:00:00.000'), 'YYYY')).toBe('2013');
	});
});
