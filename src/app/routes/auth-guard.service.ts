import { Injectable } from '@angular/core';
import {
	CanActivate,
	Router,
	ActivatedRouteSnapshot,
	RouterStateSnapshot,
	Route,
} from '@angular/router';
import { Store } from '@ngrx/store';

import { getState } from 'helpers';
import { StateInterface } from 'store';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(
		private router: Router,
		private store: Store<StateInterface>,
	) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		const url: string = state.url;
		this.router.navigate(['/login']);
		return false;
	}
}
