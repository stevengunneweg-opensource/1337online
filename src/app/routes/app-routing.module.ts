import { NgModule } from '@angular/core';
import {
	RouterModule,
	Routes,
	RouterStateSnapshot,
} from '@angular/router';
import {
	StoreRouterConnectingModule,
	routerReducer,
	RouterStateSerializer,
} from '@ngrx/router-store';

import { AuthGuard } from './auth-guard.service';
import { environment } from 'environments/environment';
import { RouterStateUrl } from 'interfaces';
import {
	HomePageComponent,
	ScoresPageComponent,
	NotFoundPageComponent,
} from 'pages';

class CustomSerializer implements RouterStateSerializer<RouterStateUrl> {
	serialize(routerState: RouterStateSnapshot): RouterStateUrl {
		let route = routerState.root;

		while (route.firstChild) {
			route = route.firstChild;
		}

		const { url, root: { queryParams }} = routerState;
		const { params } = route;

		// Only return an object including the URL, params and query params
		// instead of the entire snapshot
		return { url, params, queryParams };
	}
}

const appRoutes: Routes = [
	{
		path: '',
		component: null,
		children: [
			{
				path: '',
				pathMatch: 'full',
				component: HomePageComponent,
			}, {
				path: 'scores',
				pathMatch: 'full',
				component: ScoresPageComponent,
			}, {
				path: 'scores/:name',
				pathMatch: 'full',
				component: ScoresPageComponent,
			}, {
				path: '404',
				pathMatch: 'full',
				component: NotFoundPageComponent,
			}, {
				// if an invalid URL is given, redirect to:
				path: '**',
				redirectTo: '404',
			},
		],
		runGuardsAndResolvers: 'always',
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(
			appRoutes,
			{
				onSameUrlNavigation: 'reload',
				enableTracing: false,
			},
		),
		StoreRouterConnectingModule.forRoot({
			stateKey: 'router',
		}),
	],
	exports: [
		RouterModule,
	],
	providers: [
		{ provide: RouterStateSerializer, useClass: CustomSerializer },
	],
})
export class AppRoutingModule { }
