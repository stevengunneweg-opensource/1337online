import {
	Component,
	OnInit,
	Output,
	EventEmitter,
} from '@angular/core';

import {
	TranslateService,
} from '@ngx-translate/core';

declare let grecaptcha: any;

@Component({
	selector: 'app-recaptcha',
	templateUrl: './recaptcha.component.html',
})
export class RecaptchaComponent implements OnInit {
	@Output() tokenChange: EventEmitter<string> = new EventEmitter<string>();

	currentLanguage: string;
	isChangingLanguage: boolean = true;

	constructor(
		private translate: TranslateService,
	) {
	}

	ngOnInit(): void {
		this.currentLanguage = this.translate.currentLang;
		if (this.currentLanguage === 'leet') {
			this.currentLanguage = this.translate.defaultLang;
		}
		setTimeout(() => {
			this.isChangingLanguage = false;
		});

		this.translate.onLangChange.subscribe((selectedLanguageObject: any) => {
			this.isChangingLanguage = true;
			this.currentLanguage = this.translate.currentLang;
			if (this.currentLanguage === 'leet') {
				this.currentLanguage = this.translate.defaultLang;
			}
			// @NOTE: Wait one tick so the dom registers the change
			setTimeout(() => {
				this.isChangingLanguage = false;
			});
		});
	}

	updateToken(token?: string): void {
		this.tokenChange.emit(token);
	}
}
