import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Store, StoreModule, ActionReducer } from '@ngrx/store';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

import { cloneDeep } from 'helpers';
import { DefaultAction } from 'interfaces';
import {
	rootReducer,
	InitialState,
	StateInterface,
} from 'store';

import { HomePageComponent as Component } from './home-page.component';


describe('Home page', () => { const state = JSON.parse(JSON.stringify(InitialState));

	let component: Component;
	let fixture: ComponentFixture <Component> ;
	let store;
	let app;

	beforeEach(async (() => {
		TestBed.configureTestingModule({
			declarations: [
				Component,
			],
			imports: [
				CommonModule,
				FormsModule,
				HttpClientModule,
				ReactiveFormsModule,
				StoreModule.forRoot(rootReducer), RouterTestingModule],
			providers: [],
			schemas: [
				CUSTOM_ELEMENTS_SCHEMA,
			],
		}).compileComponents();

		fixture = TestBed.createComponent(Component);
		store = fixture.debugElement.injector.get(Store);
		app = fixture.debugElement.componentInstance;
	}));

	it('should create the homePage', async (() => {
		expect(app).toBeTruthy();
	}));

});
