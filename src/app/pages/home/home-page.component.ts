import {
	Component,
	OnInit,
} from '@angular/core';

import {
	FormBuilder,
	FormGroup,
	FormControl,
	Validators,
} from '@angular/forms';

import {
	PostInterface,
} from 'interfaces';

import {
	PostsService,
} from 'services';

@Component({
	selector: 'app-home-page',
	templateUrl: './home-page.component.html',
})
export class HomePageComponent implements OnInit {
	leetForm: FormGroup;
	isLoading: boolean = false;
	isInfoSheetVisible: boolean = false;

	constructor(
		private formBuilder: FormBuilder,
		private postsService: PostsService,
	) {
	}

	ngOnInit(): void {
		this.leetForm = this.formBuilder.group({
			name: ['frontend_test', Validators.required],
			recaptcha: ['', Validators.required],
		});
	}

	formSubmit(event: any): void {
		this.isLoading = true;
		this.postsService.putPost(this.leetForm.value).then(() => {
			this.isLoading = false;
		}).catch(() => {
			this.isLoading = false;
		});
	}

	onRecaptchaChange(event: any): void {
		this.leetForm.patchValue({
			recaptcha: event,
		});
	}

	toggleInfoSheet(state?: boolean): void {
		if (state === undefined) {
			state = !this.isInfoSheetVisible;
		}
		this.isInfoSheetVisible = state;
	}
}
