import {
	Component,
	Inject,
	OnInit,
	Input,
} from '@angular/core';
import { Store } from '@ngrx/store';

import { cloneDeep } from 'helpers';
import { StateInterface } from 'store';

@Component({
	templateUrl: 'not-found-page.component.html',
})

/**
 * Class representing the 404 page component.
 */
export class NotFoundPageComponent implements OnInit {

	/**
	 * Class constructor.
	 * @param {Store} store
	 * @return {void}
	 */
	constructor(
		private store: Store<StateInterface>,
	) {}

	ngOnInit(): void {}
}
