import {
	Component,
	OnInit,
} from '@angular/core';

import {
	ActivatedRoute,
} from '@angular/router';

import {
	Store,
} from '@ngrx/store';

import {
	StateInterface,
} from 'store';

@Component({
	selector: 'app-scores-page',
	templateUrl: './scores-page.component.html',
})
export class ScoresPageComponent implements OnInit {
	currentTimespan: string;
	username: string;

	constructor(
		private route: ActivatedRoute,
		private store: Store<StateInterface>,
	) {}

	ngOnInit(): void {
		this.store.select('ui', 'scores').subscribe((storeScoresPage: any) => {
			this.currentTimespan = storeScoresPage;
		});

		this.route.params.subscribe((routeParams: any) => {
			this.username = routeParams.name;
		});
	}

	switchTimespan(timespan?: string): void {
		this.store.dispatch({
			type: 'ScoresPageSet',
			payload: timespan,
		});
	}
}
