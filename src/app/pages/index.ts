export { HomePageComponent } from './home/home-page.component';
export { ScoresPageComponent } from './scores/scores-page.component';
export { NotFoundPageComponent } from './not-found/not-found-page.component';
