import {
	Injectable,
} from '@angular/core';

import {
	HttpClient,
	HttpHeaders,
} from '@angular/common/http';

import {
	Store,
} from '@ngrx/store';

import {
	StateInterface,
} from 'store';

import {
	Moment,
} from 'utilities';

import {
	ApiHelper,
} from 'helpers';

import {
	PostInterface,
	KnownTimespansEnum,
} from 'interfaces';

@Injectable({
	providedIn: 'root',
})
export class PostsService {

	constructor(
		private http: HttpClient,
		private store: Store<StateInterface>,
	) {
	}

	getPosts(timespan?: string, date?: string): Promise<any> {
		return new Promise<any>((resolve: any, reject: any) => {
			let endpoint: string = `${ApiHelper.endpoint}/posts`;

			if (date && !timespan) {
				timespan = KnownTimespansEnum.default;
			}

			if (timespan) {
				if (!KnownTimespansEnum.hasOwnProperty(timespan)) {
					timespan = KnownTimespansEnum.default;
				}
				endpoint += `/${timespan}`;
			}

			if (date) {
				endpoint += `/${date}`;
			}

			this.http.get(endpoint, {
				headers: ApiHelper.getHeaders(),
			}).subscribe((result: any) => {
				if (!result.success) {
					reject(result.message);
					return;
				}

				const entries: PostInterface[] = result.data.entries.map((entry: any): PostInterface => {
					return this.convertToPostFormat(entry);
				});
				resolve(entries);
			}, (error: any) => {
				reject(error);
			});
		});
	}

	putPost(data: { name: string, recaptcha: string }): Promise<any> {
		return new Promise<any>((resolve: any, reject: any) => {
			const body: any = {
				name: data.name,
				recaptcha: data.recaptcha,
			};
			this.http.put(`${ApiHelper.endpoint}/posts`, body, {
				headers: ApiHelper.getHeaders(),
			})
			.subscribe((result: any) => {
				console.log(result);
				resolve();
			}, (error: any) => {
				console.log(error);
				reject();
			});
		});
	}

	convertToPostFormat(entry: any): PostInterface {
		return {
			name: entry.name,
			time: {
				datetime: entry.time.datetime,
				datetimeformat: entry.time.datetimeformat,
			},
		};
	}
}
