import {
	Injectable,
} from '@angular/core';

import {
	HttpClient,
	HttpHeaders,
} from '@angular/common/http';

import {
	Store,
} from '@ngrx/store';

import {
	Moment,
} from 'utilities';

import {
	ApiHelper,
} from 'helpers';

import {
	TimeInterface,
} from 'interfaces';

@Injectable({
	providedIn: 'root',
})
export class TimeService {

	constructor(
		private http: HttpClient,
		private store: Store<string>,
	) {
		this.getTime().then((result: any) => {
			this.store.dispatch({
				type: 'TimeSet',
				payload: Moment(result.datetime).toISOString(),
			});
		}).catch((error: any) => {
			console.log(error);

			this.store.dispatch({
				type: 'TimeSet',
				payload: Moment().toISOString(),
			});
		});
	}

	startClock(): void {
		setInterval(() => {
			this.store.dispatch({
				type: 'TimeIncrement',
			});
		}, 1000);
	}

	getTime(): Promise<TimeInterface> {
		return new Promise<any>((resolve: any, reject: any) => {
			const endpoint: string = `${ApiHelper.endpoint}/time`;

			this.http.get(endpoint, {
				headers: ApiHelper.getHeaders(),
			}).subscribe((result: any) => {
				if (!result.success) {
					reject(result.message);
					return;
				}
				resolve(result.time);
			}, (error: any) => {
				reject(error);
			});
		});
	}
}
