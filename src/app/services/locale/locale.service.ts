import {
	Injectable,
} from '@angular/core';

import {
	TranslateService,
} from '@ngx-translate/core';

import {
	Store,
} from '@ngrx/store';

import {
	StateInterface,
} from 'store';

@Injectable({
	providedIn: 'root',
})
export class LocaleService {
	localeData: any;

	constructor(
		private translate: TranslateService,
		private store: Store<StateInterface>,
	) {
		this.store.select('data', 'locale').subscribe((storeLocale: any) => {
			if (storeLocale) {
				this.localeData = storeLocale;

				this.translate.addLangs(storeLocale.availableLocales);
				this.translate.setDefaultLang(storeLocale.defaultLocale);

				this.translate.use(storeLocale.currentLocale);
			}
		});
	}

	getAvailableLocales(): string[] {
		if (!this.localeData) {
			return [];
		}
		return this.localeData.availableLocales;
	}

	setBrowserLocale(): void {
		this.store.dispatch({
			type: 'LocaleBrowserSet',
			payload: this.translate.getBrowserLang(),
		});
	}

	setLocale(locale?: string): void {
		if (locale === undefined) {
			locale = this.translate.getBrowserLang();
		}
		this.store.dispatch({
			type: 'LocaleSet',
			payload: locale,
		});
	}
}
