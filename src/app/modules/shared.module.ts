import {
	NgModule,
	CUSTOM_ELEMENTS_SCHEMA,
} from '@angular/core';

import {
	CommonModule,
	CurrencyPipe,
} from '@angular/common';

import {
	BrowserModule,
} from '@angular/platform-browser';

import {
	ReactiveFormsModule,
	FormsModule,
} from '@angular/forms';

import {
	RouterModule,
} from '@angular/router';

import {
	HttpClient,
} from '@angular/common/http';

import {
	TranslateModule,
	TranslateLoader,
} from '@ngx-translate/core';

import {
	TranslateHttpLoader,
} from '@ngx-translate/http-loader';

import {
	PostsService,
} from 'services';

import {
	HeaderComponent,
	ScoreListComponent,
} from 'components';

import {
	MomentPipe,
	TimePipe,
} from 'pipes';


// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (createTranslateLoader),
				deps: [HttpClient],
			},
		}),
	],
	declarations: [
		// Components
		HeaderComponent,
		ScoreListComponent,
		// Pipes
		MomentPipe,
		TimePipe,
	],
	providers: [
		// Pipes
		CurrencyPipe,
		// Services
		PostsService,
	],
	exports: [
		TranslateModule,
		// Components
		HeaderComponent,
		ScoreListComponent,
		// Pipes
		MomentPipe,
		TimePipe,
	],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
	],
})

export class SharedModule {}
