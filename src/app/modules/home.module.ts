import {
	NgModule,
	CUSTOM_ELEMENTS_SCHEMA,
} from '@angular/core';

import {
	CommonModule,
} from '@angular/common';

import {
	ReactiveFormsModule,
} from '@angular/forms';

import {
	SharedModule,
} from './shared.module';

import {
	NgxCaptchaModule,
} from 'ngx-captcha';

import {
	HomePageComponent,
} from 'pages';

import {
	RecaptchaComponent,
} from 'controls';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		SharedModule,
		NgxCaptchaModule.forRoot({
			reCaptcha2SiteKey: '6LfNi1QUAAAAACCJUzFWBH_A3kJotYCdP3gvtXmL',
		}),
	],
	declarations: [
		HomePageComponent,
		RecaptchaComponent,
	],
	providers: [],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
	],
})
export class HomeModule {}
