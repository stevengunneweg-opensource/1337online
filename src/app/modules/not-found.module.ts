import {
	NgModule,
	CUSTOM_ELEMENTS_SCHEMA,
} from '@angular/core';

import {
	CommonModule,
} from '@angular/common';

import {
	NotFoundPageComponent,
} from 'pages';

import {
	SharedModule,
} from './shared.module';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
	],
	declarations: [
		NotFoundPageComponent,
	],
	providers: [],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
	],
})
export class NotFoundModule {}
