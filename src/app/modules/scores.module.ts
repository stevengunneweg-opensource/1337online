import {
	NgModule,
	CUSTOM_ELEMENTS_SCHEMA,
} from '@angular/core';

import {
	CommonModule,
} from '@angular/common';

import {
	SharedModule,
} from './shared.module';

import {
	ScoresPageComponent,
} from 'pages';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
	],
	declarations: [
		ScoresPageComponent,
	],
	providers: [],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
	],
})
export class ScoresModule {}
