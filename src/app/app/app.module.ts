import {
	BrowserModule,
} from '@angular/platform-browser';

import {
	LOCALE_ID,
	NgModule,
	CUSTOM_ELEMENTS_SCHEMA,
} from '@angular/core';

import {
	HttpClientModule,
} from '@angular/common/http';

// Ngrx store
import {
	StoreModule,
	ActionReducer,
} from '@ngrx/store';

import {
	StoreDevtoolsModule,
} from '@ngrx/store-devtools';

import {
	rootReducer,
} from 'store';

// Core
import {
	environment,
} from 'environments/environment';
import {
	AppComponent,
} from './app.component';

// Services
import { TimeService } from 'services';

// Routes
import { AppRoutingModule } from 'routes';

// Modules
import { HomeModule } from '../modules/home.module';
import { ScoresModule } from '../modules/scores.module';
import { NotFoundModule } from '../modules/not-found.module';

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		AppRoutingModule,
		BrowserModule,
		HttpClientModule,
		StoreModule.forRoot(rootReducer), !environment.production ? StoreDevtoolsModule.instrument({
			maxAge: 10,
		}) : [],
		// Page modules
		HomeModule,
		ScoresModule,
		NotFoundModule,
	],
	providers: [
		TimeService,
	],
	bootstrap: [
		AppComponent,
	],
})
export class AppModule {}
