import {
	Component,
} from '@angular/core';

import {
	LocaleService,
	TimeService,
} from 'services';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
})
export class AppComponent {

	constructor(
		private localeService: LocaleService,
		private timeService: TimeService,
	) {
		this.localeService.setBrowserLocale();
		timeService.startClock();
	}
}
