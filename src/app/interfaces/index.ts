export {
	DefaultAction,
} from './actions/actions.interface';

export {
	RouterStateUrl,
} from './routes/routes.interface';

export {
	PostInterface,
	KnownTimespansEnum,
} from './posts/posts.interface';

export {
	TimeInterface,
} from './time/time.interface';
