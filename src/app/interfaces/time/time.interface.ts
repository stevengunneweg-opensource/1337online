export interface TimeInterface {
	datetime: string;
	datetimeformat: string;
}
