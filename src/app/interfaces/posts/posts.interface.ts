import {
	TimeInterface,
} from 'interfaces';

export interface PostInterface {
	name: string;
	time: TimeInterface;
}

export enum KnownTimespansEnum {
	default = 'day',
	day = 'day',
	week = 'week',
	month = 'month',
	year = 'year',
	all = 'all',
}
