import { Action } from '@ngrx/store';

/**
 * The unsafe Action type. Doesn't know what the payload is. Could me manipulated.
 * @NOTE This is here for fallback. Whenever you work in a file that still uses this Action Type. Please replace it.
 * @extends {Action}
 */
export interface DefaultAction extends Action {
	/**
	 * The payload
	 * @property {payload} any
	 */
	payload?: any;
}
