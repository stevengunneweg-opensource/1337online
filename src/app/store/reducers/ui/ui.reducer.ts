import {
	ActionReducer,
	combineReducers,
} from '@ngrx/store';

import {
	scoresPageReducer,
} from './';

// Store ui state
export const InitialState = {
};

// Store ui reducer
export const uiReducers: ActionReducer<any> = combineReducers({
	scores: scoresPageReducer,
});
