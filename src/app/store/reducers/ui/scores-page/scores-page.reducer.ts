import {
	ActionReducer,
} from '@ngrx/store';

import {
	DefaultAction,
} from 'interfaces';

import {
	rehydrateState,
	saveState,
} from 'helpers';

export const initialScoresPageState: any = 'day';

export const scoresPageReducer: ActionReducer<any> = (state: any = initialScoresPageState, action: DefaultAction) => {
	const storeKey: string = 'scoresPage';
	const {
		type,
		payload,
	} = action;
	let newState: string = state;

	switch (type) {
		case '@ngrx/store/init':
			newState = rehydrateState(storeKey);
			break;
		case 'ScoresPageSet':
			newState = payload;
			break;
	}

	saveState(storeKey, newState);

	return newState;
};
