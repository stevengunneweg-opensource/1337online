export {
	dataReducers,
} from './data/data.reducer';

export {
	uiReducers,
} from './ui/ui.reducer';
