export {
	localeReducer,
	initialLocaleState,
} from './locale/locale.reducer';

export {
	timeReducer,
	initialTimeState,
} from './time/time.reducer';

export {
	postsReducer,
	initialPostsState,
} from './posts/posts.reducer';
