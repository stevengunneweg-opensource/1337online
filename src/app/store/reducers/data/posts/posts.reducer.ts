import {
	ActionReducer,
} from '@ngrx/store';

import {
	DefaultAction,
	PostInterface,
} from 'interfaces';

import {
	rehydrateState,
	saveState,
} from 'helpers';

export const initialPostsState: PostInterface[] = [];

export const postsReducer: ActionReducer<PostInterface[]> = (state: PostInterface[] = initialPostsState, action: DefaultAction) => {
	const storeKey: string = 'posts';
	const {
		type,
		payload,
	} = action;
	let newState: any = state;

	switch (type) {
		case '@ngrx/store/init':
			newState = rehydrateState(storeKey);
			break;
		case 'PostsSet':
			newState = payload;
			break;
	}

	saveState(storeKey, newState);

	return newState;
};
