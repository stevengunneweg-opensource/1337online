import {
	ActionReducer,
} from '@ngrx/store';

import {
	DefaultAction,
} from 'interfaces';

import {
	rehydrateState,
	saveState,
	cloneDeep,
} from 'helpers';

export const initialLocaleState: any = {
	availableLocales: ['en', 'nl', 'leet'],
	defaultLocale: 'en',
	currentLocale: '',
};

export const localeReducer: ActionReducer<any> = (state: any = initialLocaleState, action: DefaultAction) => {
	const storeKey: string = 'locale';
	const {
		type,
		payload,
	} = action;
	let newState: any = state;

	switch (type) {
		case '@ngrx/store/init':
			newState = rehydrateState(storeKey);
			break;
		case 'LocaleBrowserSet':
			newState = cloneDeep(state);
			if (!state.currentLocale) {
				if (state.availableLocales.indexOf(payload) > -1) {
					newState.currentLocale = payload;
				} else {
					newState.currentLocale = newState.defaultLocale;
				}
			}
			break;
		case 'LocaleSet':
			newState = cloneDeep(state);
			if (state.availableLocales.indexOf(payload) > -1) {
				newState.currentLocale = payload;
			}
			break;
	}

	saveState(storeKey, newState);

	return newState;
};
