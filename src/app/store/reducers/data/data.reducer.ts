import {
	ActionReducer,
	combineReducers,
} from '@ngrx/store';

import {
	localeReducer,
	timeReducer,
	postsReducer,
} from './';

// Store data state
export const InitialState = {
};

// Store data reducer
export const dataReducers: ActionReducer<any> = combineReducers({
	locale: localeReducer,
	time: timeReducer,
	posts: postsReducer,
});
