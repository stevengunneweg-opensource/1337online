import {
	ActionReducer,
} from '@ngrx/store';

import {
	DefaultAction,
} from 'interfaces';

import {
	rehydrateState,
	saveState,
} from 'helpers';

import {
	Moment,
} from 'utilities';

export const initialTimeState: string = Moment().toISOString();

export const timeReducer: ActionReducer<string> = (state: string = initialTimeState, action: DefaultAction) => {
	const storeKey: string = 'time';
	const {
		type,
		payload,
	} = action;
	let newState: string = state;

	switch (type) {
		case '@ngrx/store/init':
			newState = rehydrateState(storeKey);
			break;
		case 'TimeSet':
			newState = payload;
			break;
		case 'TimeIncrement':
			newState = Moment(state).add(1, 'second').toISOString();
			break;
	}

	saveState(storeKey, newState);

	return newState;
};
