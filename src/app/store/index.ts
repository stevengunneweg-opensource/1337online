export {
	InitialState,
	rootReducer,
} from './root.reducer';

export {
	StateInterface,
} from './state.interface';

