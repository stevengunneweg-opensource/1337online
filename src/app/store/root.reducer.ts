import {
	ActionReducerMap,
} from '@ngrx/store';

import {
	routerReducer,
} from '@ngrx/router-store';

import {
	StateInterface,
} from './state.interface';

import {
	dataReducers,
	uiReducers,
} from './reducers';

// Store root state
export const InitialState = {
};

// Store root reducer
export const rootReducer: ActionReducerMap<StateInterface> = {
	router: routerReducer,
	data: dataReducers,
	ui: uiReducers,
};
