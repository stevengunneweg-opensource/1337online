import {
	Store, State,
} from '@ngrx/store';

import 'rxjs/add/operator/take';

import {
	StorageHelper,
} from '../storage/storage.helper';

import {
	StateInterface,
} from 'store';

import {
	environment,
} from 'environments/environment';

export const rehydrateAction = '@ngrx/store/init';
export const cloneDeep = require('lodash.clonedeep');

/**
 * See https://github.com/ngrx/store, the part below
 * 'Migrating from Store v1.x' for more information on
 * why @ngrx removed the getStore method from their API.
 * @param {Store} store
 * @return {StateInterface} state
 */
export const getState = (store: Store<StateInterface> ): StateInterface => {
	let state: StateInterface;
	store.take(1).subscribe((storeState: StateInterface) => state = storeState);
	return cloneDeep(state);
};

// Checks if the value exists and sends it back.
export const rehydrateState = (key: string) => {
	let rehydratedState: any;

	if (StorageHelper.keyExists(`${key}`)) {
		rehydratedState = cloneDeep(StorageHelper.getJson(`${key}`));
	}

	return rehydratedState;
};

export const saveState = (key: string, json: any): void => {
	StorageHelper.storeAsJson(`${key}`, json);
};
