import { ComponentFixture } from '@angular/core/testing';
import {} from 'jasmine';
import { By } from '@angular/platform-browser';

/**
 * Class representing the TestHelper helper.
 */
export class TestHelper {

	/**
	 * Returns an element to by its [attr.test-id] selector.
	 * @param {ComponentFixture} fixture
	 * @param {string} selector - e.g. 'header' with [attr.test-id]="'header"
	 * @return {void}
	 */
	static getElementByTestId(fixture: ComponentFixture<any>, selector: string): any {
		return fixture.nativeElement.querySelector(`[test-id="${selector}"]`);
	}

	/**
	 * Expect an element to exist by its [attr.test-id] selector.
	 * @param {ComponentFixture} fixture
	 * @param {string} selector - e.g. 'header' with [attr.test-id]="'header"
	 * @return {void}
	 */
	static expectToExist(fixture: ComponentFixture<any>, selector: string): void {
		expect(fixture.nativeElement
			.querySelector(`[test-id="${selector}"]`) === null)
			.toBe(false);
	}

	/**
	 * Expect an element NOT to exist by its [attr.test-id] selector.
	 * @param {ComponentFixture} fixture
	 * @param {string} selector - e.g. 'header' with [attr.test-id]="'header"
	 * @return {void}
	 */
	static expectNotToExist(fixture: ComponentFixture<any>, selector: string): void {
		expect(fixture.nativeElement
			.querySelector(`[test-id="${selector}"]`) === null)
			.toBe(true);
	}

	/**
	 * Expect an element to exist and contain a string.
	 * @param {ComponentFixture} fixture
	 * @param {string} selector - e.g. 'header' with [attr.test-id]="'header"
	 * @param {string} needle
	 * @return {void}
	 */
	static expectToContain(fixture: ComponentFixture<any>, selector: string, needle: string): void {
		expect(fixture.nativeElement
			.querySelector(`[test-id="${selector}"]`)
			.innerHTML.trim())
			.toContain(needle);
	}

	/**
	 * Expect an element's attribute to exist and contain a string.
	 * @param {ComponentFixture} fixture
	 * @param {string} selector - e.g. 'header' with [attr.test-id]="'header"
	 * @param {string} attribute - e.g. 'href'
	 * @param {string} needle
	 * @return {void}
	 */
	static expectAttributeToContain(fixture: ComponentFixture<any>, selector: string, attribute: string, needle: string): void {
		expect(fixture.nativeElement
			.querySelector(`[test-id="${selector}"]`)
			.getAttribute(attribute))
			.toContain(needle);
	}

	/**
	 * Get an injected property's instance.
	 * @param {ComponentFixture} fixture
	 * @param {object} property - e.g. an injected service
	 * @return {object}
	 */
	static getInjected(fixture: ComponentFixture<any>, property: any): any {
		return fixture.debugElement.injector.get(property);
	}
}
