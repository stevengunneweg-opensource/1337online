import {
	environment,
} from 'environments/environment';

/**
 * Class representing the Storage helper.
 */
export class StorageHelper {

	/**
	 * Checks if a key exists.
	 * @return {boolean}
	 */
	static keyExists(key: string): boolean {
		return sessionStorage.getItem(`${environment.version}/${key}`) !== null;
	}

	/**
	 * Sets a key and a value.
	 * @return {void}
	 */
	static set(key: string, value: string): void {
		sessionStorage.setItem(`${environment.version}/${key}`, value);
	}

	/**
	 * Gets a value by key.
	 * @return {any}
	 */
	static get(key: string): any {
		return sessionStorage.getItem(`${environment.version}/${key}`);
	}

	/**
	 * Removes sessionStorage by key.
	 * @return {void}
	*/
	static remove(key: string): void {
		sessionStorage.removeItem(`${environment.version}/${key}`);
	}

	/**
	 * Stores JSON in the sessionStorage.
	 * @param {string} key
	 * @param {JSON} json
	 * @return {void}
	 */
	static storeAsJson(key: string, json: any): void {
		sessionStorage.setItem(`${environment.version}/${key}`, JSON.stringify(json));
	}

	/**
	 * Gets JSON from the sessionStorage.
	 * @param {string} key
	 * @return {JSON}
	 */
	static getJson(key: string): void {
		return JSON.parse(sessionStorage.getItem(`${environment.version}/${key}`));
	}

	/**
	 * Checks if a key exists and if its empty.
	 * @return {boolean}
	 */
	static keyIsNullOrEmpty(key: string): boolean {
		const value = sessionStorage.getItem(`${environment.version}/${key}`);

		if (value !== null && value.length > 0) {
			return false;
		}
		return true;
	}
}

export default StorageHelper;
