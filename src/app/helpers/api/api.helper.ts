import {
	HttpHeaders,
} from '@angular/common/http';

import {
	environment,
} from 'environments/environment';

export class ApiHelper {
	static endpoint: string = environment.endpoint;

	static getHeaders(headers?: any): HttpHeaders {
		if (!headers) {
			headers = {};
		}
		headers['content-type'] = 'application/json';
		headers['application-id'] = environment.applicationId;

		return new HttpHeaders(headers);
	}
}
