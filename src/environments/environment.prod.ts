export const environment = {
	production: true,
	version: require('../../package.json').version,

	endpoint: 'http://localhost:3000',
	applicationId: 'e5ef78afa91ae8f2f6ec419cca3a7a4f',
};
